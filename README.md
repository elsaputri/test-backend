# TOKDIS Code Test - Service Transaction

## Overview

The task is to build a basic transaction on ecommerce in PHP / Golang / Node JS / Java.

## Domain and Business Rules

A Transaction has a buyer, seller,list products with variants, total transaction, shipping fee and discount for shipping fee based on total transaction(qty * price).

Each products has a name,code, description,minimum buy qty for discount parameter and a list of variant that are included in the product. 
Variant also have a variant id, varints description(Ex: S/M/L for shirt), price (every variant can have different price) and qty.
Buyer and seller has name, address road, zipcode, province,city, district and village.

- Each products can give discount maximum 3% from minimum buy qty * variant price.
- Maximum discount for every transaction is 5% from total transaction before shipping fee.
- Total transaction, shipping fee and discount must be automated calculated.

## Tasks

1. Create the entities, aggregates and value objects to model this domain.

2. Create a database that will store this data

3. Implement the ability to read and write the object data to/from the database

4. Create an API that supports create, update and delete use cases of all entities

5. Create an API for create transaction. This Api must give data response invoice number, total transaction, shipping fee and discount.

6. Add unit test to your code.

Optional:
1. Implement TDD.

2. Use OOP approach if possible.

3. If required implements some design patterns.

4. Keep your code clean and simple.

5. Add documentation on your code.

## Notes

Consider appropriate business rules that would apply in a real world ecommerce scenario and ensure invariants are enforced.

Use the type system to ensure correctness of the data in the application layer, and use appropriate types in the database.

We should not be able to persist invalid data.

For get real estimated shipping price you can used API from rajaongkir.com, you can access from this link https://rajaongkir.com/dokumentasi/starter. please register first for get api-key. 

## Evaluation

The application should be run on docker, setup should be a simple.

Please commit regularly with descriptive commit messages along the way.

Share your git repo privately with [@ilyas2](https://github.com/ilyas2) on github (we dont want answers getting public)

You are free to choose framework but keep in mind if we cant get it to run in under 5 minutes on our macs we probably arent going to get to review it :)